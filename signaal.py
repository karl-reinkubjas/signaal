import matplotlib.pyplot as plt
from astropy.io import fits
from matplotlib.colors import LogNorm

#==============================================================================
# Loeb fits failist sisse andmed ning väljastab seal olevad andmed.
#==============================================================================
def open_fits_file(name):
    hdu_list = fits.open(name)  # Open FITS file
    hdu_list.info()  # List the contents of the FITS file
    h = hdu_list[0].header
    x = h['NAXIS1']
    y = h['NAXIS2']
    return hdu_list[0].data, x, y


# Loetakse sisse pildi andmed ning pildi suurus.
data, size_x, size_y = open_fits_file("starfield_5.fits")

ave = 0

# Leitakse kogu pildi iga rea maksimumide summa
for i in range(size_y):
    ave += max(data[i])

# Leitakse maksimumide keskmine
ave /= size_y

stardata = []
suurus = []
#==============================================================================
# X-i keskmistamine
#==============================================================================
for i in range(size_y):
    star_row =[]
    suurus_row = []
    x = 0
    c = 0
    # Keskmistame iga rea
    for s in range(size_x):
        # Kui piksli väärtus on väiksem kui keskmine, paneme nulliks - taustamüra  
        if data[i][s] < ave:
            data[i][s] = 0
            if x != 0:
                star_row.append(x//c)
                # Loeme kokku, mittu pikslit on tähes
                suurus_row.append(c)
                x = 0
                c = 0
        # Kui rea väärtus on üle keskmise, pannakse väärtuseks "1" - täht
        else:
            data[i][s] = 1
            x += s
            c += 1
    # Kui reas on rohkem kui üks täht, uurime, kas on vaja keskmistada
    if len(star_row) > 1:
        new = []
        new_suurus = []
        qq = 0
        qw = 0
        qq_suurus = 0
        # Määrame eeslmisele sellies väärtuse, et igal juhul if klausel töötaks
        eelmine = -100
        for ab in range(len(star_row)):
            # Kui kahe tähe vahe on väikem kui 5 salvestame andmed, et leida kekmist
            if star_row[ab] - eelmine <= 5:
                qq += star_row[ab]
                qq_suurus += suurus_row[ab]
                qw += 1
            else:
                # Kuna täht lõppes leiame selle keskmise
                if (qq != 0):
                    new.append(qq//qw)
                    new_suurus.append(qq_suurus)
                    
                # Nullime andmed uue tähe keskmistamiseks
                qq = star_row[ab]
                qq_suurus = suurus_row[ab]
                qw = 1
                eelmine = star_row[ab]
        # Lisame listi viimase tähe antud reas
        new.append(qq//qw)
        new_suurus.append(qq_suurus)              
        stardata.append(new)
        suurus.append(new_suurus)
    else:
        # Lisame listi rea, kui selles asub ainult üks täht
        stardata.append(star_row)
        suurus.append(suurus_row)

sumx = []
sumy = []
last_y = []
suurus_k = []
count = []
tahed = []
#==============================================================================
# Y-koordinaadi keskmistamine
#==============================================================================
for i in range(len(stardata)): 
    # keskmistab ainult ridasid, kus on tähed      
    if stardata[i] != []:
        # Kui eelimne rida oli tühi lisame andmed otse
        if len(sumx) == 0:
            sumx = stardata[i]
            for ee in range(len(stardata[i])):
                sumy.append(i)
                count.append(1)
                last_y.append(i)
                suurus_k.append(suurus[i][ee])
        
        lisa = True
        # käime terve rea läbi, ee on x- väärtus
        for ee in range(len(stardata[i])):
            # lisan sellised tähed listi, mis algavad siis kui juba mõnda tähte loetakse
            # Kui eelnev rida ei ole tühi, kontrollime, kas x-väärtus on juba sumx listis, kui ei lisame
            for k in range(-5,5,1):
                if stardata[i][ee] + k in sumx:
                    for m in range(len(sumx)):
                        if stardata[i][ee] + k == sumx[m] and i == sumy[m]:
                            lisa = False
                            break
            if lisa == True:
                sumx.append(stardata[i][ee])
                sumy.append(i)
                count.append(1)
                last_y.append(i)
                suurus_k.append(suurus[i][ee])
            # käime terve last_y listi läbi; ff võtab indeksi väärtused 0,1,2,3....
            for ff in range(len(last_y)):
                # Kui last_y listis on element, mis järgneb reale, millel hetkel asume ning x-väärtus sumx listis
                # samal kohal ei erine uuritava x-väärtusest rohkem kui 5 ühikut minnakse if lausesse.
                if last_y[ff] == i-1 and abs(sumx[ff]-stardata[i][ee]) <= 5 :
                    # Arvutame jooksva x-väärtuse keskmise
                    sumx[ff] = (sumx[ff]*count[ff]+stardata[i][ee])//(count[ff]+1)
                    # Arvutame jooksva y-väärtuse keskmise
                    sumy[ff] = (sumy[ff]*count[ff]+i)/(count[ff]+1)
                    count[ff] += 1
                    last_y[ff] = i
                    suurus_k[ff] += suurus[i][ee]

    # Kui rida on tühi ja sumx ei ole null - paneb tähtede listi asjad ja teeb listid tühjaks
    elif len(sumx) != 0:
        for abc in range(len(sumx)):
            tahed.append([sumx[abc], int(sumy[abc]), suurus_k[abc]])
        # Nullime listid, et uute tähtede leidmiseks saaks neid listi panna
        sumx = []
        sumy = []
        count = []
        last_y = []
        suurus_k = []

# Käime listi korra veel üüle ning viskame sarnased väärtused välja
new_tahed = [tahed[0]]
for m in range(1,len(tahed)):
    juurde = True
    for n in range(len(new_tahed)):
        if abs(tahed[m][0] - new_tahed[n][0]) < 8 and abs(tahed[m][1] - new_tahed[n][1]) < 8:
            juurde = False
    if juurde:
        new_tahed.append(tahed[m])

tahed = new_tahed

print(tahed)
print(len(tahed))

fig = plt.figure()
plt.imshow(data, cmap='gray', norm=LogNorm())


for c in tahed:
    c1 = plt.Circle((c[0], c[1]), c[2]/5, color=(0, 0, 1))
    fig.add_subplot(111).add_artist(c1)

plt.show()
