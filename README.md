# Rühmatöö #

Rühmatöö eesmärgiks on genereeritud tähistaeva pildilt üles leida tähed ja määrata nende suurused. 

Projekti kood on kirjutatud pythonis. Lisaks standartsetele teekidele on lisaks vaja

- [astropy](http://www.astropy.org/)
- numpy
- matplotlib
